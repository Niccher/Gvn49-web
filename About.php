<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="icon" href="./assets/user-properties.ico">

    <title>My School</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link href="assets/css/Pasted.css" rel="stylesheet">

  </head>
  <body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Gvn 49</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
        <li class="active"><a href="About.php">About Us</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


    <ol class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        Home
      </li>
    </ol>


  <div class="row">

  <style type="text/css">
  body{
    background-color:#f1f1f1;
}
.ourTeam-hedding{
    background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('../demo/hands-joined.jpg');
    height:200px;
    width:100%;
    background-size:100% 100%;
    background-repeat:no-repeat;
    background: linear-gradient(rgba(0,0,0,.10)); 
}
.ourTeam-hedding h1{
    color:#fff;
    padding:70px 0px;
    font-weight:900;
    z-index: 1;
}
.ourTeam-box{
    border-top: 6px solid #5DDDD3;
    background-color: #FFFFFF;
}
.section1{
    padding: 30px 0px 0px 0px;
}
.section1 img{
    border:5px solid #959595;
    padding:5px;
    border-radius:50%;
    height:170px;
    width: 170px;
}
.section2 h1{
    font-size:20px;
    color:#C1C1C1;
    margin:0px;
    border-bottom:2px solid #959595;
    display: inline-block;
    padding: 10px 0px;
}
.section2 p{
    background-color: #959595;
    display: inline-block;
    font-weight: bold;
    color:#fff;
    padding: 5px 15px;
    border-radius: 3px;
    margin-top: -20px;    
}
.section2 span{
    color:#959595;
}
.section2 label{
    background-color: #959595;
    padding: 8px 10px 8px 10px;
    color:#fff;
    font-weight: normal;
    margin: 30px 0px 35px 0px;
    border-radius: 2px;
}
.section3 p{
    padding: 10px 15px 0px 15px;
    font-size:12px;
    color:#c3c9c9;
}
.section4{
    padding:10px 0px 50px 0px;
}
.section4 i{
    color:#fff;
    padding:3px;
    border-radius: 2px; 
    font-size: 12px;
    background-color: #959595;
    cursor: pointer;
}
.section-info{
    border-top:6px solid #959595;
}
.section-danger{
    border-top:6px solid #959595;
}
.section-info .section1 img{
    border:5px solid #959595;
    padding:5px;
}
.section-info .section2 p{
    background-color: #959595;
    color:#fff;
}
.section-danger .section2 p{
    background-color: #959595;
    color:#fff;
}
.section-danger .section1 img{
    border:5px solid #959595;
    padding:5px;
}
.section-info .section4 i{
    background-color: #959595;  
}
.section-danger .section4 i{
    background-color: #959595;  
}
.section-info .section2 h1{
    border-bottom:2px solid #959595;
}
.section-danger .section2 h1{
    border-bottom:2px solid #959595;
}
</style>

  <div class="container section-ourTeam">
  <div class="col-md-12 row">
  	  <div class="jumbotron">
      <h1>This Is Our Team</h1>
      <p>Gvn49 Workforce Little desc Here</p>
  </div>
  </div>
  <div class="row">
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-danger ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="/demo/meet01.png">
        </div>
        <div class="col-md-12 section2">
          <p>Admin 1</p><br>
          <h1>Level <strong>Admin</strong></h1><br>
        </div>
        <div class="col-md-12 section3">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-danger ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="/demo/meet01.png">
        </div>
        <div class="col-md-12 section2">
          <p>Admin 2</p><br>
          <h1>Level <strong>Admin</strong></h1><br>
        </div>
        <div class="col-md-12 section3">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-sm-4 col-xs-12">
      <div class="row section-danger ourTeam-box text-center">
        <div class="col-md-12 section1">
          <img src="/demo/meet01.png">
        </div>
        <div class="col-md-12 section2">
          <p>Admin 3</p><br>
          <h1>Level <strong>Admin</strong></h1><br>
        </div>
        <div class="col-md-12 section3">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
          </p>
        </div>
        <div class="col-md-12 section4">
          <i class="fa fa-facebook-official" aria-hidden="true"></i>
          <i class="fa fa-twitter" aria-hidden="true"></i>
          <i class="fa fa-envelope" aria-hidden="true"></i>
        </div>
      </div>
    </div>

  </div>
</div>

  </div>

    <footer id="footer">
      <p>Gvn 49 &copy;2018</p>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  
</body></html>