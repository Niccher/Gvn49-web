<?php
session_start();
include 'Itika.php';
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" href="./assets/user-properties.ico">

    <title>Gvn 49</title>
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <link href="assets/css/Pasted.css" rel="stylesheet">

  </head>
  <body>

<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Gvn 49</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="About.php">About Us</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

<?php

?> 

    <ol class="breadcrumb">
      <li>
        <i class="ace-icon fa fa-home home-icon"></i>
        Home
      </li>
      <li>
        <i class="fa fa-angle-double-right"></i>
      </li>
      <li>
        <i class="fa fa-users"></i>
        Guest Privileges
      </li>
    </ol>

	<div class="row" style="margin-top: 20px;">   
		<div class="jumbotron col-lg-12">
			<div class="col-md-9 col-md-offset-1 col-sm-2 col-sm-offset-1 col-xs-10 col-xs-offset-1">
				<div class="text-center">
		    	<div class="table-responsive">  
                 <table class="table table-bordered table-striped table-hover">  
                      <tr>  
                           <th width="8%">Count</th>   
                           <th width="10%">Time</th>
                           <th width="15%">Region</th>  
                           <th width="40%">Message</th>  
                      </tr>  
			        <?php  
			            $query = "SELECT * FROM `tbl_Inbox` ";  
			            $result = mysqli_query($conni, $query);  
			            while($row = mysqli_fetch_array($result))  {  
			        ?>  
                      <tr>  
                           <td><?php echo $row["Count"]; ?></td>    
                           <td><?php echo $row["Moment"]; ?></td>  
                           <td><?php echo $row["Region"]; ?></td> 
                           <td><?php echo $row["Message"]; ?></td>  
                      </tr>  
                      <?php  
                      }  
                      ?>  
                 </table>  
                </div>
                <p>
                    <a class="btn btn-primary btn-lg" href="Login.php" role="button">Compose My Comment</a>
                </p>
		    </div>
			</div>
		</div>
    </div> 

    <footer id="footer">
      <p>Gvn 49 &copy;2018</p>
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>

</body>
</html>