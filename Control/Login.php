<?php
session_start();
?>
<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Gvn 49</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link rel="icon" href="./assets/user-properties.ico">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assets/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
	<link href="assets/css/Pasted.css" rel="stylesheet">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
</head>

<?php

include 'Itika.php';

  if (isset($_POST['Senda'])) {

    $eml=strip_tags(($_POST['Eml']));
    //$eml=mysql_real_escape_string(($_POST['Eml']));
    $pwd=strip_tags(($_POST['Pswd']));
    
    $_SESSION['emilo']=$eml;
    $_SESSION['upasso']=$pwd;

      $Comp="SELECT * FROM `tbl_Users` WHERE `Email`='$eml' AND `Password`='$pwd' ";
      $Insd=mysqli_query($conni,$Comp) or die("\n".mysqli_error($conni));


      $coun=mysqli_num_rows($Insd);
      $rw=mysqli_fetch_assoc($Insd);
      if ($coun == 1) {
        echo "<script>window.location ='index.php'</script>";
      }else{
        $err=mysqli_error($conni);
        echo '<div class="alert alert-danger alert-dismissable">';
        echo $err;
        echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>';
      }
  }
?> 

<body>
	<!-- WRAPPER -->
	<div id="row">
		<div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-1 col-xs-10 col-xs-offset-1">
        
        	<div class="content">
				<div class="header">
					<h1>
						<div class="logo text-center">Gvn49</div>
					</h1>
					<h1>
						<div class="logo text-center">Administrative Portal</div>
					</h1>
					<h3>
						<div class="logo text-center">View Questions and Comments Posted and Reply to Comments and Questions Alike</div>
					</h1>
					<p class="lead">Verify account</p>
				</div>
				<form class="form-auth-small" action="Login.php" method="post">
					<div class="form-group">
						<label for="signin-email" class="control-label sr-only">Email</label>
						<input type="email" name="Eml" class="form-control" id="signin-email" placeholder="Email">
					</div>
					<div class="form-group">
						<label for="signin-password" class="control-label sr-only">Password</label>
						<input type="password" name="Pswd" class="form-control" id="signin-password" value="" placeholder="Password">
					</div>
					<div class="form-group clearfix">
						<label class="fancy-checkbox element-left">
							<input type="checkbox">
							<span>Remember me</span>
						</label>
					</div>
					<input type="submit" class="btn btn-primary btn-lg btn-block" name="Senda" value="Login">
				</form>
			</div>
        
</div>
	<!-- END WRAPPER -->
</body>

</html>
