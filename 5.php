<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="bootstrap.css">
<script type="text/javascript">
	$(document).ready(function(){
		$("#myModal").modal('show');
	});
</script>

<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        	<div class="modal-header">
        		<h3>Fill The Form Below To Create An Account With Us</h3>
	        </div>
	        <div class="modal-body">
	       	<div class="panel panel-info">
                <div class="panel-heading" style="background: #adc2ad;">
                    <div class="panel-title">Sign Up</div>
                    <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#">Sign In</a></div>
                </div>  
                <div class="panel-body" >
                    <form id="signupform" class="form-horizontal" role="form">
                        
                        <div id="signupalert" style="display:none" class="alert alert-danger">
                            <p>Error:</p>
                            <span></span>
                        </div>

                        <div class="form-group">
                            <label for="firstname" class="col-md-3 control-label">Name</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="firstname" placeholder="First Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">Email</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email" placeholder="Email Address">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="lastname" class="col-md-3 control-label">Region</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="lastname" placeholder="Region">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="passwd" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Password</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="passwd" placeholder=" Confirm Password">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label for="icode" class="col-md-3 control-label">Invitation Code</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="icode" placeholder="">
                            </div>
                        </div>

                        <!---->
                        <div class="box-body">
				              <div class="box-group" id="accordion">
				                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
				                <div class="panel box box-danger">
				                  <div class="box-header with-border">
				                    <h4 class="box-title">
				                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
				                        Terms And Conditions.
				                      </a>
				                    </h4>
				                  </div>
				                  <div id="collapseTwo" class="panel-collapse collapse">
				                    <div class="box-body">
				                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				                      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				                      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				                    </div>
				                  </div>
				                </div>
				              </div>
				            </div>
                        <!---->
                        
                        <div style="border-top: 1px solid #999; padding-top:20px"  class="form-group">
                            
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btn-fbsignup" type="button" class="btn btn-primary"><i class="icon-facebook"></i>Continue</button>
                            </div>                                           
                                
                        </div>
                    </form>
                 </div>
            </div>  
	        </div>
	        <div class="modal-footer"></div> 
        </div>  		